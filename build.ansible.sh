#!/bin/bash
PURPOSE="build a custom ansible installation using pip & virtualenv"

# -- auto: path variables
scriptSelf=$0;
scriptName=${scriptSelf##*/}; scriptCallDir=${scriptSelf%/*};
cd $scriptCallDir; scriptFullDir=$PWD; scriptFullPath=$scriptFullDir/$scriptName;
scriptTopDir=${scriptFullDir%/*}
# -- /auto: path variables

# -- helper functions
# - default output functions
function printVersion { echo -e "\033[1m# $scriptName version $VERSION\033[0m\n"; [[ -n "$1" ]] && exit ${1}; }
function printOutput  { [[ -e $outFile ]] && out=$(cat $outFile); [[ -n "$out" ]] && echo -e "$out" || echo -e "NO OUTPUT"; }
function printTitle   { echo; msg="$*";linesno=$(( ${#msg} + 4 ));msglines=$(for i in `seq $linesno`;do echo -n "=";done);echo -e "$msglines\n# $msg #\n$msglines"; }
function printOut     { echo -en "$@"; }
function printOk      { echo -en "\033[32m$*\033[0m"; }
function printInfo    { echo -en "\033[34m$*\033[0m"; }
function printError   { echo -en "\033[31m$*\033[0m"; }
# - default exit functions
function exitError    { echo -e "\n\033[31m[$HOSTNAME] ERROR: ${1:-"Unknown Error"}. Exiting...\033[0m" 1>&2; exit ${2:-1}; }
function exitInfo     { echo -e "\n\033[34m[$HOSTNAME] INFO: ${1:-"Thank you for using $scriptName"}.\033[0m" 1>&2; exit ${2:-1}; }
# -- /helper functions


# -- work variables
[[ -z "$BUILD_DIR" ]] && BUILD_DIR="$HOME/build"
BUILD_VER="latest"


# -- script usage
if [[ "$1" = "-h" || "$1" = "--help" ]];then
  printInfo "\n# $PURPOSE\n"
  printInfo "  Usage: $scriptName [ansible-version (default=$BUILD_VER)]\n\n"
  exit 0
elif [[ "$1" = "-v" || "$1" = "--versions" ]];then
  BUILD_VER="VERSIONS"
elif [[ "$1" =~ [0-9]\.*[0-9] ]];then
  BUILD_VER="$1"
fi


# -- optional user prompt
printTitle "BUILDING ANSIBLE [$BUILD_VER] in [$BUILD_DIR]"
[[ -z "$1" ]] && printInfo "(Press ENTER to CONTINUE, CTRL+C to ABORT)" && read a


# -- install dependencies
printTitle "INSTALLING DEPENDENCIES"
for pkgfile in $scriptFullDir/os.packages.*
do
  bash $pkgfile
done


# -- check os type
OS_TYPE_FILE=/tmp/os.type
[[ -x /usr/bin/sw_vers ]] && OS_TYPE_FILE=/tmp/os.type
if [[ -e $OS_TYPE_FILE ]];then
  OS_TYPE=$(cat $OS_TYPE_FILE)
  printOk "\n--\n* OS dependencies installed successfully (OS TYPE: $OS_TYPE)\n--\n"
else
  exitError "ERROR while installing OS dependencies"
fi


# -- check build version
if [[ "$BUILD_VER" = "VERSIONS" ]];then
  clear
  printInfo "# $PURPOSE"
  printTitle "AVAILABLE VERSIONS"
  pip install ansible== 2>&1| tr "," "\n" | tr -d ")" | grep "^ *[0-9]"
  exit 99
fi


# -- exit n error from here
set -e


# -- install build pip requirements
printTitle "UPDATING PIP REQUIREMENTS"
PIP_PACKAGES="pip virtualenv"
if which sudo >/dev/null 2>&1;then
  case "$OS_TYPE" in
    osx)
      sudo pip install --upgrade $PIP_PACKAGES
      ;;
    *)
      sudo -E pip install --upgrade $PIP_PACKAGES
      ;;
    esac
else
  pip install --upgrade $PIP_PACKAGES
fi


# -- setup virtualenv
BUILD_NAME="ansible"
BUILD_VENV="$BUILD_DIR/$BUILD_NAME"
BUILD_ACTIVATE="$BUILD_VENV/bin/activate"
# cleanup venv dir if already exists
[[ -d $BUILD_VENV ]] && rm -rf $BUILD_VENV
# create work dir if not present yet
[[ -d $BUILD_DIR ]] || mkdir -p $BUILD_DIR
# create virtualenv
virtualenv $BUILD_VENV
# activate virtualenv
source $BUILD_ACTIVATE

# -- install ansible in venv
printTitle "INSTALLING ANSIBLE IN VIRTUALENV"
if [[ "$BUILD_VER" = "latest" ]];then
  pip install --upgrade ansible
else
  pip install ansible==$BUILD_VER
fi

# -- install required pip packages in venv
printTitle "INSTALLING EXTRA MODULES IN VIRTUALENV"
pip install --upgrade -r $scriptFullDir/pip.requirements.txt

# -- make virtualenv relocateable
virtualenv --relocatable $BUILD_VENV

# -- create ansible config
mkdir -p $BUILD_VENV/cfg
# copy ansible config
cp $scriptFullDir/files/ansible.cfg $BUILD_VENV/cfg/
# copy ansible default environment
cp $scriptFullDir/files/ansible.env $BUILD_VENV/cfg/
# copy project default environment
cp $scriptFullDir/files/project.env $BUILD_VENV/cfg/
# print ansible version
printOk "\n--\n* virtualenv [$BUILD_VENV] installed successfully\n--\n"
ansible --version 2>/dev/null
deactivate


# -- adding external tools
if [[ "$SKIP_EXTERNAL_TOOLS" == "true" ]];then
  printTitle "SKIPPING DOWNLOAD OF EXTERNAL TOOLS"
  echo "environment variable SKIP_EXETERNAL_TOOLS=true"
  RELEASE_FILENAME_SUFFIX=".notools"
else
  printTitle "DOWNLOADING EXTERNAL TOOLS"
  # export needed variables
  export BUILD_DIR BUILD_VER BUILD_VENV OS_TYPE
  # include version
  for download_script in $scriptFullDir/tools.download.*
  do
    bash $download_script
    echo
  done
  RELEASE_FILENAME_SUFFIX=""
fi


# -- run ansible setup tasks
# add ansible env vars to activate script
$BUILD_VENV/bin/ansible-playbook -e "BUILD_VENV=$BUILD_VENV" -e "activate_file_path=$BUILD_ACTIVATE" $scriptFullDir/build.setenv.yml 2>/dev/null
printOk "\n--\n* virtualenv [$BUILD_VENV] set up successfully\n--\n"

# -- create tgz release
RELEASE_FILENAME="${BUILD_NAME}-${BUILD_VER}${RELEASE_FILENAME_SUFFIX}.${OS_TYPE}.tgz"
cd $BUILD_DIR
tar czf $RELEASE_FILENAME $BUILD_NAME/
printOk "\n--\n* archiv ${BUILD_DIR}/$RELEASE_FILENAME created successfully\n--\n"

# --upload tgz release
if [[ -n "$FTP_UPLOAD_URL" ]];then
  curl -s -T ${BUILD_DIR}/$RELEASE_FILENAME "${FTP_UPLOAD_URL}"
  printOk "\n--\n* archiv $RELEASE_FILENAME successfully uploaded\n--\n"
fi


# eof
