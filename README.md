## ABOUT this repository

#### `ANSIBLE-BUILDTOOLS`
the **ansible-basetools** repository hosts a build environment for creating a ***"portable ansible installation"*** using:
  - the `python pip` package manager
  - the `python virtualenv` module
  - external tools like `goss`, `monit`or `terraform`

The build script will then create a tar.gz archive of the created environment. On the current repository, automatic builds for the supported OS plaftorms are enabled and the resulting archives are available for download from the release site [http://ansible.iops.at] (http://ansible.iops.at/).

#### `SUPPORTED OS PLATFORMS`
Builds has been tested on following OS releases/distributions:

  - **el6**: CentOS or RedHat Enterprise Linux 6.x
  - **el7**: CentOS or RedHat Enterprise Linux 7.x
  - **lts**: Ubuntu LTS versions 14.04 and 16.04
  - **osx**: Mac OSX versions 10.x
  - **rpi**: Raspberry Pi 3 running piCore 8.x


<br />
## BUILDING RELEASE(s)

#### `REQUIREMENTS`
  - you will **NEED** `root-privileges` and `internet or OS-repository access` to build environments (for setting up dependencies before starting the build process).
  - the ansible virtual environment relies on an existing `python 2.x` installation, so python must already be installed on the build system.


#### `BUILDING`
  - clone the current repository or download the source code using the corresponding buttons in the file/branch view
  ```
  #> git clone https://gitlab.com/iops-ansible-base/ansible-buildenv.git
  ```

  - use the `build.ansible.sh` script to build a portable ansible release for the corresponding OS release/distribution:
  ```
  #> ./build.ansible.sh --help
  # build a custom ansible installation using pip & virtualenv
    Usage: build.ansible.sh [ansible-version (default=latest)]
  ```
  By default, the `latest` release will be built, but you can build any of the available version (just run the script with the `--versions` switch to list them).

  - The virtual environment and the release archive will be created under the `$HOME/build` directory that is auto-created during the build process.



<br />
## INSTALLING RELEASE(s)

#### `REQUIREMENTS`
  - the ansible virtual environment relies on an existing `python 2.x` installation, so python must already be installed on the target system.
  - you **DO NOT** need `root-privileges` to run this environment (ex: ansible can be installed in user's home directory).

#### `INSTALLATION`
  - Pick your ansible version from the [release download site] (http://ansible.iops.at/) or build your own from source.
  - Download and unpack the corresponding release file:
  ```
  curl -#k http://ansible.iops.at/ansible-2.2.el6.tgz|tar xz
  ```

  - This will create a directory named `ansible` in the current folder that you could use immediately by using full paths:
  ```
  #> ./ansible/bin/ansible localhost -a 'echo "Hello World'
  ```

  - But if you want a full integration into the current environment, `activate` the virtualenv first. You will then be able to run all ansible commands directly:
  ```
  #> source ./ansible/bin/activate
  (ansible) #> ansible --version
  ```
  Just type `deactivate`if you want to leave the virtual environment
  ```
  (ansible) #> deactivate
  #>
  ```

<br />
## COPYRIGHT & LICENSING
(c) copyright Fidy Andrianaivo.

All custom scripts and files in that repository are provided under the [BSD 2-CLause](LICENSE) License
