# docker file for ansible base tools
# VERSION: 2018-02-12-001
# escape=`


# -- build application (temporary stage)
# base image to use
FROM alpine AS buildcontainer

# args: build dirs
ARG BUILD_DIR="/opt"
ARG BUILD_TMP="/builds"
# args: minimum required os packages
ARG DOCKER_OS_PACKAGES="bash"

# envs: build dirs
ENV BUILD_DIR=${BUILD_DIR}
ENV BUILD_TMP=${BUILD_TMP}

# copy build files
COPY . ${BUILD_TMP}/

# install minimal os deps
RUN apk update && apk add ${DOCKER_OS_PACKAGES}

# build virtualenv
RUN /bin/bash ${BUILD_TMP}/build.ansible.sh

# cleanup built archive
RUN rm -rf ${BUILD_DIR}/*.tgz




# -- build production image (final stage)
# base image
FROM alpine
MAINTAINER Fidy Andrianaivo (fidy@andrianaivo.org)
LABEL Description="ANSIBLE base image"

# args: build dst dir
ARG BUILD_DIR="/opt"
# args: required os packages
ARG DOCKER_OS_PACKAGES="bash curl git python tzdata"
# args: timezone
ARG TZ="Europe/Vienna"

# envs: timezone
ENV TZ ${TZ}

# setup virtualenv dependencies
RUN apk update && apk add ${DOCKER_OS_PACKAGES} && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# get final application release
COPY --from=buildcontainer ${BUILD_DIR} /opt/

# use wrapper script as entrypoint
ENTRYPOINT ["/opt/ansible/bin/run.sh"]


# eof
